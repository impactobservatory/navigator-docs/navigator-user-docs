.. _Tags:

==========
Layer Tags
==========

Tags allow you t filter the layers returned using the tags search bar in the filters panel. You can view the tags associated with a layer in a layer’s description box using the info button in the legend. 

Users with editor or admin permission to a workspace can access the layer admin to create and manage tags for new or existing layers. Create tags while editing or creating a layer. Create a new tag by typing into the tags field. You can also select an existing tag from the same workspace. To delete a tag, open the tag manager and click the X next to the tag name. The tag will only be deleted from the single layer.
