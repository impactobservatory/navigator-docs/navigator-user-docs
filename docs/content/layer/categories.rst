.. _Layer Categories:

================
Layer Categories
================

A set of standard categories and sub-categories is included in the
Navigator platform. In the map view, the filters panel can be used to
return layers from specific categories. Layer categories are assigned
in a workspace admin when creating or editing a layer by an editor or
admin user. 

The list of categories is pre-defined and cannot be changed.
See:ref:`Tags` to create unique filters for layers.
