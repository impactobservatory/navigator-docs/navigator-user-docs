===================
STAC Best Practices 
===================

**1. Do build Collections around distinct sources and methods**

    * Do not use spatially or temporally-specific information in Collection name

    * Do use Collection and Item properties to define spatial/temporal information

    * Do place Assets in a new Collection if they come from a different source or use a different method

    * Do thoroughly describe methods or link to description in Collection description.

**2. Do use consistent Asset names, particularly across Items in a Collection to create a single layer from tiled data in Navigator**

    * Use the same name for Assets throughout multiple Items in a Collection for tiled datasets so that Navigator can mosaic all Assets from a single Collection into a single layer. 

**3. Do create Items with multiple Assets to show multiple dimensions of a result**

    * Do think of this the same way as adding another band to a raster:

        * Does it match in time and space?

        * Does it add related info?

        * If the answer to both is "yes!", add it. (e.g. asset 1 = softmax, asset 2 = prediction)

    * Do not add assets that were created with various methodologies (e.g. different compositing logic) -> new method, new Collection!

**4. Do precisely specify temporal/spatial bounds on Items.**

    * Do set ``start_date`` and ``end_date`` Item properties instead of ``datetime`` when applicable.

    * Do not worry about precise temporal/spatial bounds on Collections. It is better to set them "too big" than "too small", so you can easily add more Items.

**5. Do think about how your Collection fits in with other products (if it can be extended, if there's already a naming convention, ...) when setting a name.**

