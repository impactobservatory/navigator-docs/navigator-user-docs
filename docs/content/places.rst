======
Places
======
.. _places:

Your private workspace contains all of your custom :term:`places <place>`, or .geojson files. 

How to Create a New Place 
-------------------------

- In the places page of your workspace admin, click CREATE NEW PLACE.  
- Name the place
- Select the :term:`place type <place type>`
- Click GENERATE A SLUG NAME to automatically create a :term:`slug <slug>` name, this is a unique ID for the place
- Next, click Choose File. Select a GeoJSON from your local machine. Navigator requires a GeoJSON file under 2MB, and a coordinate reference system in WGS 84 EPSG 4326.
- SAVE AND VIEW DETAILS. 
- Turn on the Publish toggle for the place to be accessible from the Map View. It is also recommended to turn on the Featured toggle if you would like the place to appear at the top of search results. 
- Click the green RECALCULATE ALL button if the metrics are not automatically triggered 

.. image:: ../images/create-place.png
   :width: 700 px
   :alt: Create New Place Menu

.. note:: Indicator load times: Once you save a place, indicator calculations are automatically triggered. The smaller the place, the faster the indicator will calculate, usually in a few seconds or minutes. If an indicator fails to load after a few hours, the place may be too large of an area, or there may be an issue with your GeoJSON. Check that the GeoJSON is a closed polygon in a GIS tool or try breaking up the place into smaller shapes.  

.. note:: GeoJSON upload requirements: 1. file size under 2MB, 2. coordinate reference system in WGS 84 EPSG 4326 3. contain valid geometries 4. polygon vector file. If you get an error saving the file, check that your shape meets the file requirements. If you continue to have problems, reach out to our team for technical support. 

Download GeoJSON for a Place
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: ../images/dwnld-geojson.png
   :width: 700 px
   :alt: GeoJSON Download Menu

Navigator allows users to download the GeoJSON file for a place to their local machine. This feature maintains continuity between analysis on the platform and any additional analysis or cartography GIS users desire to perform with other applications. 

To download the GeoJSON file for a place:

- In the places admin, search for the place
- Click the VIEW AND UPLOAD SHAPE button, found above the preview map of the shape
- Click Download GeoJSON


Hide a Place from the Map View: 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you would like to temporarily hide a certain place in your private workspace from the Map View:

- Search for the place in the places admin 
- Turn off the Published toggle. This action can be undone by clicking the Published button again. When a place is unpublished it will remain in the places admin, but it will not appear in search results in the Map View. 

Permanently Delete a Place:	
~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Enter the Places admin
- Search for and select the place you want to delete 
- Scroll down to the bottom of the place's metadata
- Click DELETE PLACE 



