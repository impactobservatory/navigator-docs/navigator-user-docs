======================
Accessing Your Account
======================

.. _creating-user-account:

As an early user of the :term:`Navigator <navigator>` platform, you have access to a private :term:`workspace <workspace>` on the platform visible only to you, your organization, and Impact Observatory. Your private workspace will contain your organization’s :term:`places <place>` of interest and any :term:`layers <layer>` you provide the IO team to upload.


Account Registration
--------------------

Visit `navigator.impactobservatory.com <https://navigator.impactobservatory.com/>`_ to sign up for Navigator. After signing up, confirm your account via email and notify your team’s account admin or someone at Impact Observatory. The account admin at your company or at Impact Observatory will add your email address to your team’s private workspace.
 

Log Into Your Account
---------------------

.. image:: ../images/log-in.png
        :width: 500 px
        :alt: Log-in page. 

Once you have an account, you can access the platform via `navigator.impactobservatory.com <https://navigator.impactobservatory.com/>`_. If you forget your password, a reset link can be sent to your email address on file.

If you are unable to access your account or receive a 403 error, make sure your account email address has been added to your organization’s workspace by the account manager at Impact Observatory.
 

Passwords
---------

Change Password
~~~~~~~~~~~~~~~
You can change your password from your Profile menu.


Forgot Password
~~~~~~~~~~~~~~~
If you forgot your password, you use the Forgot Password feature on the Sign-In form.

A message to confirm you want to change your password is sent to your email address on file.
