============
The Map View
============

Upon launching the platform, you are brought to the :term:`map view <map view>`. The map view is where you can search for places or :term:`layers<layer>` to extract insights from Navigator’s catalog of environmental data and indicators.


.. image:: ../images/map-view.png
    :width: 800 px
    :alt: The Map View. 



Places Tab on Navigator
=======================
	
Early users have access to a catalog of places within the Navigator workspace. Navigator contains over 4,900 places representing all recognized countries and regions to measure and monitor environmental trends in any region of the world.

Search for a Place
------------------

Type in the name of a country or region while in the PLACES tab. Note that only the places within your selected workspaces will appear in the search results. Select the specific workspaces from the Map View button that you would like to search through. 

You can also search within the PLACES tab using the filters box. Expand the Filters button, and select your filter of interest. You then can select the desired place from the search result list.

Click on a location from the search results to zoom the map into the area of interest and calculate the standard set of environmental indicators. 

.. tip:: 
  See :ref:`Download Indicator Results` to learn how to take advantage of indicator results for further analysis. See `Create a New Place <https://navigator-user-docs.readthedocs.io/en/latest/workspaces/workspaces.html#create-a-new-place>`_ to learn how to run indicators over your areas of interest.
 


Where did Impact Observatory Source the Place files on Navigator from? 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All shapes are from `Who’s On First <https://whosonfirst.org/>`_, a shape gazetteer that compiles a variety of open data sources to create a complete global set of country and region shapes.

.. tip::
   Visit `Who’s On First <https://whosonfirst.org/>`_ to find and download a free GeoJSON for nearly any place on Earth. Upload a GeoJSON to your private workspace on Navigator to calculate custom metrics over your area of interest. See `*Create a Place* <https://whosonfirst.org/>`_ to learn how. 


Your Places
-----------

Within your private workspace, you can upload custom places that are of interest to you and your organization. The standard set of indicators are calculated for any places uploaded to your private workspace.

Once a place in your private workspace is published, it will appear in the search results of the places tab for all users in your organization. If you are not seeing your places in the search results, you may need to select your workspace from the drop down menu from the MAP VIEW button. If you have no workspaces selected, the search applies to all workspaces. 



Layers Tab on Navigator 
=======================

All users have access to a standard catalog of global environmental datasets, referred to as :term:`layers<layer>`, to monitor and track environmental trends anywhere in the world. The set of global layers are located within the Navigator workspace. Additional layers can be added to your private workspace.


Search for a layer
------------------
From the LAYERS tab, type in the name or part of a name of a dataset. Note that only layers within your selected workspaces will appear in the search results. Select the specific workspaces from the Map View button that you would like to search through. You can also use the FILTERS button to filter the search results by a dataset’s associated category.


Layer Controls
~~~~~~~~~~~~~~

Toggle on a layer to view it on the map. 

Each layer legend contains additional controls to edit the layer transparency, hide the layer, read a description of the layer and source information, and remove the layer from the map.

Users may add multiple layers to the map simultaneously and alter the order they appear in using the leftmost dots icon in the legend. Try changing the transparency of one layer to see how information from two layers overlaps.


.. image:: ../images/layer-controls.png
   :width: 400 px
   :alt: Layer controls can be found on the legend of a layer. 

 

About Layers on Navigator
~~~~~~~~~~~~~~~~~~~~~~~~~

Additional information is available by clicking on the “i” button in a layer’s legend including:

  - A description of the data
  - Source
  - Suggested Citation
  - Download the Data hyperlink
  - Read the Paper hyperlink
  - License 

The information in this description box can be edited by users with editor, admin, or owner permissions.

Download the Data from an Original Source
-----------------------------------------

If data is available for download from an original source you can find a link to the original source in the description box.  

To find the link for a certain layer:

- Toggle on the layer in the Map View
- Click the “i” button in the legend to open a pop up window with the description box. Find the Download the Data hyperlink for access to a download source. 

If a Download the Data hyperlink is not listed, the layer is not available for download from an outside source.
   



Clip and Export Data 
====================


You can use the :term:`clip and export layer <clip and export>` button to download data for a specific place for use in a slide deck, report, or document:

.. image:: ../images/clip-export.png
   :width: 400 px
   :alt: Clip and Export Layers

- Select the PLACES tab in the left-hand panel, search for and select your desired place.
- Click on the “More Options” button next to the title of the place to open the “Click and Export Layer” drop down.
- A new tab will open in the left-hand panel. Search for and select your layer of interest. If your layer contains reference layers, such as multiple years or multiple versions of the data, you will need to select one of the reference layers from a second drop down menu. 
- Select the download format. Currently, the platform only supports PNG format. 
- Click DOWNLOAD to clip and export the layer for only the selected place. The PNG image will save to the Downloads folder on your local computer. Some layers are not available for download due to license restrictions.



Map Controls
============

The MAP CONTROLS button in the bottom right corner of the map allows you to edit the :term:`basemap <basemap>`, zoom in or out, turn on or off labels and roads, and recenter the map over the selected place. The satellite basemap, labels, and roads are provided by Mapbox.

.. image:: ../images/map-controls.png
   :width: 200 px
   :alt: Map controls pop-up window, found in the bottom right corner. 


Basemaps
--------

The :term:`basemap <basemap>` can be set to a grayscale map style or a satellite basemap from Mapbox. No specific dates for imagery can be provided by Mapbox at this time. Satellite basemap images are from a variety of satellites and range from a few years to a few months old. Because there is no way to determine the date the images on the satellite basemap were taken, it should not be used exclusively to verify results, rather only as a general reference.

