==========
Workspaces
==========

What is a Workspace?
====================

A :term:`workspace <workspace>` provides a secure work area within the Navigator platform where your organization's data can be added and shared with a set of specified users. 

Organizations with a private workspace can upload content to Navigator visible only to you, your organization, and Impact Observatory. Custom datasets and places of interest can be uploaded for analysis to a private workspace.

The Navigator Workspace
=======================

All users are able to view the main Navigator workspace where you will find a standard set of places, data layers, and indicators. The Navigator workspace includes shapes for all countries and regions of the world, a set of global environmental datasets, and a set of nine indicators to automatically calculate summary statistics over any country, region, or shape in your private workspace.

.. image:: ../images/select-workspaces.png
    :width: 600 px
    :alt: How to select workspaces.


Select a Workspace
==================
Alongside the Impact Observatory logo in the top left panel, you can see all of the workspaces you are a part of by clicking the "Map View" drop down button. By default, if no workspaces are selected, the search behaves as if all of the workspaces were selected. Selecting one workspace will limit the places and layers in the search results to content only in that workspace.

.. note:: We do not recommend unchecking the Navigator workspace as this will temporarily hide the indicator visualizations from the left-hand panel when viewing a place. If no indicators are visible when you select a place, make sure the Navigator workspace is selected, or that no workspaces are selected.

Access a Workspace Admin
========================

.. image:: ../images/access-admin.png
    :width: 600 px
    :alt: How to access the admin.
	
Users with editor, admin or owner permissions can access a private :term:`workspace admin <workspace admin>`:

- Click via the MAP VIEW drop down menu. 
- Click the ADMIN button next to the workspace where the content you would like to manage are stored. 


.. image:: ../images/nav-home.png
    :width: 600 px
    :alt: Home page.

When you enter the workspace admin, you will be on the home page where you can find a summary dashboard of the content in your workspace including the total number of layers and places. Click on the drop down menu in the left-hand panel to select the type of content you would like to manage. Places, widgets, layers, users, dashboards, and workspaces can all be managed in the admin.



Manage Users in Your Workspace
------------------------------
.. _User Permissions:

User Permissions
~~~~~~~~~~~~~~~~
As a member of a private workspace your account is assigned to one of four permission levels: owner, admin, editor, or viewer. Roles and permissions are used to define what individual users can do within a workspace. User permissions can be edited at any time. 

- :term:`Viewers <viewer>` - The viewer permisission is best for users who only want to view data in the Map View, with no GIS experience. Viewers can view all workspace content on the map view, including places, indicators, and layers. Viewers can also download indicator results, clip and export data from a layer for any place, create collections of places, and clip and export data from a collection. Viewers do not have access to the admin tool from where layers, places, and users can be added, edited and removed. 

- :term:`Editors <editor>` - Editors have all viewer permissions, and can manage content via the admin tool. Editors do not have access to add or remove users. While not required, experience working with GIS or design software will better enable editors when:
   
   - uploading places
   - downloading or removing places
   - uploading layers from a Spatio-Temporal Asset Catalog (STAC)
   - editing layer and place metadata, such as the title, categories, and description boxes
   - editing layer styles and colors 
   - editing layer legend styles and colors  
     
- :term:`Admins <admin>` - have all viewer and editor permissions, as well as the ability to add or remove users to the private workspace, and assign permission settings for other users in the workspace.

- :term:`Owners <owner>` - are defined when creating a private workspace. A platform admin may want to provide an owner who is an Impact Observatory employee to each private workspace on Navigator for quick access to a client's workspace for technical assistance.

Adding and Removing Users
~~~~~~~~~~~~~~~~~~~~~~~~~

Only admins and owners have the ability to add or remove :term:`users <user>`. 

.. image:: ../images/add-user.png
   :width: 800 px
   :alt: Add User Menu

To add a new user to a private workspace:

- Send the new user the following link to sign up for Navigator: https://navigator.impactobservatory.com/ 
- Once the new user has verified their account via email, the existing workspace admin needs to add their email address to the private workspace. Access this page from the MAP VIEW drop down menu >> ADMIN >> HOME >> Users >> Add new user to Navigator
- Use the "Select role" drop down menu to choose the permission of the new user's account. See `User Permissions`_ to determine which role to provide the user. 
