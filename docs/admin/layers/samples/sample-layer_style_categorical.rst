Sample Config File - Categorical
================================

Categorical Datasets

Example Legend from the IO LULC maps

.. code-block:: json
  :linenos:

  "legendConfig": {
    "type": "basic",
    "items": [
      {
        "value": "0",
        "name": "nodata",
        "color": "#ffffff"
      },
      {
        "value": "1",
        "name": "water",
        "color": "#419bdf"
      },
      {
        "value": "2",
        "name": "trees",
        "color": "#397d49"
      },
      {
        "value": "3",
        "name": "grass",
        "color": "#88b053",
        "hideFromLegend": true
      },
      {
        "value": "4",
        "name": "flooded vegetation",
        "color": "#7a87c6"
      },
      {
        "value": "5",
        "name": "crops",
        "color": "#e49635"
      },
      {
        "value": "6",
        "name": "scrub",
        "color": "#dfc35a",
        "hideFromLegend": true
      },
      {
        "value": "7",
        "name": "built area",
        "color": "#c4281b"
      },
      {
        "value": "8",
        "name": "bare ground",
        "color": "#a59b8f"
      },
      {
        "value": "9",
        "name": "snow/ice",
        "color": "#a8ebff"
      },
      {
        "value": "10",
        "name": "clouds",
        "color": "#616161"
      },
      {
        "value": "11",
        "name": "rangeland ",
        "color": "#e3e2c3ff"
      }
    ]
  }
